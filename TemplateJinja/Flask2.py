from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def index():
    mesi = ['Gen','Feb','Mar','Apr','Mag','Giu','Lug','Ago','Sett','Ott','Nov','Dic']
    #Dizionario chiave mese epoi abbiamo vari valori
    weather = {
        'Gen': {'min': 38, 'max': 47, 'rain': 6.14},
        'Feb': {'min': 38, 'max': 51, 'rain': 4.79},
        'Mar': {'min': 41, 'max': 56, 'rain': 4.5},
        'Apr': {'min': 44, 'max': 61, 'rain': 3.4},
        'Mag': {'min': 49, 'max': 67, 'rain': 2.55},
        'Giu': {'min': 53, 'max': 73, 'rain': 1.69},
        'Lug': {'min': 57, 'max': 80, 'rain': 0.59},
        'Ago': {'min': 58, 'max': 80, 'rain': 0.71},
        'Sett': {'min': 54, 'max': 75, 'rain': 1.54},
        'Ott': {'min': 48, 'max': 63, 'rain': 3.42},
        'Nov': {'min': 41, 'max': 52, 'rain': 6.74},
        'Dic': {'min': 36, 'max': 45, 'rain': 6.94}
    }
    highlight = {'min': 40, 'max': 80, 'rain': 5}

    return render_template('index.html', city='Napoli, Italy', months=mesi,weather=weather, highlight=highlight)




if __name__ == '__main__':
    app.run()
