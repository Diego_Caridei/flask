__author__ = 'Diego Caridei'
from  flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')



#Vedi file user.html tutto quello all'interno delle doppie {{è un parametro}}
@app.route('/user/<name>/<surname>')
def user(name,surname):
    #name = name il primo name fa riferimento a quello all'interno del file html
    #il secondo name al paramentro della funzione e così via
    return  render_template('user.html',name=name,surname=surname)



if __name__ == '__main__':
    app.run()

